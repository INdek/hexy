describe("Vector List", function()
  setup(function()
    require "fun"()
    VectorList = require "hexy.utils.vector_list"
    Vector = require "hump.vector"
  end)

  it("implements imutable operations", function()
    local initial = VectorList({ Vector(1, 2) })
    local initial_copy = VectorList({ Vector(1, 2) })

    local res = initial * 10

    assert.is_true(initial == initial_copy)
    assert.is_true(res ~= initial)
    assert.is_true(res ~= initial_copy)
  end);

  it("compares individual elements", function()
    local vec = VectorList({ Vector(1, 2) })
    local vec2 = VectorList({ Vector(1, 2) })
    local vec3 = VectorList({ Vector(2, 2) })

    assert.is_true(vec == vec2)
    assert.is_false(vec == vec3)
  end);

  it("rotates correctly", function()
    local vec = VectorList({
      Vector(1, 2),
      Vector(3, 4),
      Vector(5, 6)
    })
    local vec_rot = VectorList({
      Vector(1, 2):rotateInplace(math.pi),
      Vector(3, 4):rotateInplace(math.pi),
      Vector(5, 6):rotateInplace(math.pi)
    })

    zip(vec:rotate(math.pi), vec_rot):each(assert.is_equal)
  end);


  it("unpacks correctly", function()
    local vec = VectorList({
      Vector(1, 2),
      Vector(3, 4),
      Vector(5, 6)
    })

    zip(vec:unpack(), range(6)):each(assert.is_equal)
  end);

  it("is iteratable", function()
    local vec = VectorList({
      Vector(0, 0),
      Vector(1, 1),
      Vector(2, 2)
    })

    enumerate(vec):each(function(k, v)
      assert.is_equal(vec[k], v)
    end)
  end)

  it("implements the index metamethod", function()
    local vec = VectorList({
      Vector(0, 0),
      Vector(1, 1),
      Vector(2, 2)
    })

    assert.is_equal(nil, vec[0])
    assert.is_equal(Vector(0, 0), vec[1])
    assert.is_equal(Vector(1, 1), vec[2])
    assert.is_equal(Vector(2, 2), vec[3])
    assert.is_equal(nil, vec[4])
  end)

  it("correctly calculates its length", function()
    local vec = VectorList()
    local vec2 = VectorList({
      Vector(0, 0),
      Vector(1, 1)
    })

    assert.is_equal(0, vec:len())
    assert.is_equal(0, #vec)
    assert.is_equal(2, vec2:len())
    assert.is_equal(2, #vec2)
  end)

  it("tostring", function()
    local vec = VectorList({
      Vector(0, 0),
      Vector(1, 1)
    })
    assert.are.equal("(0,0), (1,1)", tostring(vec))
  end)
end)
