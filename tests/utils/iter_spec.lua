describe("Iteration", function()
  setup(function()
    iter = require("hexy.utils.iter")
  end)

  describe("group", function()
    it("correctly groups singles", function()
      assert.are.same(
        { {1}, {2}, {3}, {4} },
        iter.group({1, 2, 3, 4}, 1, 1)
      )
    end)
    it("correctly truncates elements smaller than size", function()
      assert.are.same(
        { {1, 2}, {2, 3}, {3, 4} },
        iter.group({1, 2, 3, 4}, 2, 1)
      )
    end)
    it("steps correctly on pairs", function()
      assert.are.same(
        { {1, 2}, {3, 4} },
        iter.group({1, 2, 3, 4}, 2, 2)
      )
    end)
  end)
end)
