describe("Color", function()
  setup(function()
    require "fun"()
    Color = require "hexy.utils.color"
  end)

  -- TODO: We should do this for more than just multiply
  -- We should then do the same on VectorList
  it("implements imutable operations", function()
    local initial = Color({ 0xFF, 0xFF, 0xFF, 0xFF, })
    local initial_copy = Color({ 0xFF, 0xFF, 0xFF, 0xFF, })

    local res = initial * 10

    assert.is_true(initial == initial_copy)
    assert.is_true(res ~= initial)
    assert.is_true(res ~= initial_copy)
  end);

  it("compares individual elements", function()
    local color = Color({ 0xFF, 0xFF, 0xFF, 0xFF })
    local color2 = Color({ 0xFF, 0xFF, 0xFF, 0xFF })
    local color3 = Color({ 0x00, 0x00, 0x00, 0x00 })

    assert.is_true(color == color2)
    assert.is_false(color == color3)
  end);


  it("has a working index function", function()
    local color = Color(0x00, 0x11, 0x22, 0x33)
    assert.is_equal(0x00, color[1])
    assert.is_equal(0x11, color[2])
    assert.is_equal(0x22, color[3])
    assert.is_equal(0x33, color[4])
  end);

  it("tostring", function()
    local color = Color(0x00, 0x11, 0x22, 0x33)
    assert.are.equal("0, 17, 34, 51", tostring(color))
  end)

  describe("transforms", function()
    it("can add and remove transforms", function()
      local color = Color(0x00, 0x11, 0x22, 0x33)
      assert.is_equal(0, color:transform_size())
      color:add_transform(function(s, dt) end)
      assert.is_equal(1, color:transform_size())
      color:remove_transform()
      assert.is_equal(0, color:transform_size())
    end);

    it("run on update", function()
      local color = Color(0x00, 0x11, 0x22, 0x33)
      local transform = spy.new(function(s, dt) return false end)
      local dt = 0.001
      color:add_transform(transform)
      color:update(dt)

      assert.spy(transform).was.called()
      assert.spy(transform).was.called_with(color, dt)
      assert.is_equal(1, color:transform_size())
    end)

    it("removes transforms on success", function()
      local color = Color(0x00, 0x11, 0x22, 0x33)
      local transform = function(s, dt) return true end

      color:add_transform(transform)
      color:update()

      assert.is_equal(0, color:transform_size())
    end)
  end)
end)
