describe("Drawable", function()
  setup(function()
    require "fun"()
    drawable = require("hexy.drawable")
  end)

  describe("SimpleHexagon", function()
    it("produces equidistant vertices", function()
      local distance = 10
      local hex = drawable.SimpleHexagon(distance)
      local vertices = hex:vertices()

      -- All the vertices are calculated to 0, 0
      each(function(vert)
        assert.is_equal(distance, vert:len())
      end, vertices)
    end)
  end)

  describe("Triangle", function()
    it("produces equidistant edges", function()
      local distance = 10
      local tri = drawable.Triangle(distance)
      local vertices = tri:vertices()

      assert.is_equal(distance, vertices[1]:dist(vertices[2]))
      assert.is_equal(distance, vertices[1]:dist(vertices[3]))
      assert.is_equal(distance, vertices[2]:dist(vertices[3]))
    end)
    it("produces equidistant vertices", function()
      local distance = 10
      local tri = drawable.Triangle(distance)
      local vertices = tri:vertices()

      assert.is_equal(vertices[1]:len(), vertices[2]:len())
      assert.is_equal(vertices[1]:len(), vertices[3]:len())
      assert.is_equal(vertices[2]:len(), vertices[3]:len())
    end)
  end)
end)
