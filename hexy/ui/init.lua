-- Copyright(C) 2017, Afonso Bordado <afonsobordado@az8.co>
--
-- This program is free software; you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation; either version 2 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License along
-- with this program; if not, write to the Free Software Foundation, Inc.,
-- 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

local Class = require "hump.class"
local fpsGraph = require "hexy.ui.FPSGraph"

local Ui = Class {
  init = function(self)
    self.graphs = {
      fpsGraph.createGraph(0, 0, 60, 30),
      fpsGraph.createGraph(0, 60, 60, 60),
    }
  end;

  draw = function(self)
    love.graphics.setColor(30, 192, 158)
    fpsGraph.drawGraphs(self.graphs)
  end;

  update = function(self, dt)
    fpsGraph.updateFPS(self.graphs[1], dt)
    fpsGraph.updateMem(self.graphs[2], dt)
  end;
}

return Ui
