-- Copyright(C) 2017, Afonso Bordado <afonsobordado@az8.co>
--
-- This program is free software; you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation; either version 2 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License along
-- with this program; if not, write to the Free Software Foundation, Inc.,
-- 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

local Class = require "hump.class"
local Vector = require "hump.vector"
local VectorList = require "hexy.utils.vector_list"

local Barrier = Class {
  init = function(self, height, sides, speed, distance)
    self.height = height or 30
    self.sides = sides or 6
    self.speed = speed or 10
    self.distance = distance or 100
  end;

  side_angle = function(self)
    return math.pi * 2 / self.sides
  end;

  edge = function(self, distance)
    local angle = self:side_angle() / 2
    local x = math.sin(angle) * distance
    local y = math.cos(angle) * distance
    return {
      Vector(-x, -y),
      Vector( x, -y),
    }
  end;

  gen_poly = function(self)
    local bottom = self:edge(self.distance)
    local top = self:edge(self.distance + self.height)
    return VectorList({
      bottom[1],
      bottom[2],
      top[2],
      top[1],
    })
  end;


  draw = function(self, primary_color, secondary_color)
    local poly = self:gen_poly()
    love.graphics.setColor({0xff,0xff,0xff,0xff})
    love.graphics.polygon('fill', poly:unpack())
  end;

  update = function(self, dt)
    self.distance = self.distance - self.speed * dt
  end;
}

return Barrier
