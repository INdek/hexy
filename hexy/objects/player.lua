-- Copyright(C) 2017, Afonso Bordado <afonsobordado@az8.co>
--
-- This program is free software; you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation; either version 2 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License along
-- with this program; if not, write to the Free Software Foundation, Inc.,
-- 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

local Class = require "hump.class"
local Vector = require "hump.vector"
local VectorList = require "hexy.utils.vector_list"
local Triangle = require "hexy.drawable.triangle"

local Player = Class {
  init = function(self)
    self.position = 0
    self.triangle = Triangle(20)
    self.radius = 220
    self.speed = 1.2
  end;

  pos = function(self)
    local angle = self:rot()
    local x = math.sin(angle) * self.radius
    local y = math.cos(angle) * self.radius
    return Vector(x, y)
  end;

  rot = function(self)
    return math.pi * 2 * self.position
  end;

  draw = function(self, primary_color, secondary_color)
    love.graphics.setColor( primary_color )
    self.triangle:draw(self:pos(), (-self:rot() + math.pi))
  end;

  update = function(self, dt)
    if love.keyboard.isDown("left") then
      self.position = self.position + (self.speed * dt)
    end
    if love.keyboard.isDown("right") then
      self.position = self.position - (self.speed * dt)
    end
  end;
}

return Player
