-- Copyright(C) 2017, Afonso Bordado <afonsobordado@az8.co>
--
-- This program is free software; you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation; either version 2 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License along
-- with this program; if not, write to the Free Software Foundation, Inc.,
-- 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

local Class = require "hump.class"
local Vector = require "hump.vector"
local VectorList = require "hexy.utils.vector_list"
local Triangle = require "hexy.drawable.triangle"
local Color = require "hexy.utils.color"

local Background = Class {
  init = function(self, size, sides)
    assert(size ~= nil)
    self.size = size
    self.sides = sides or 6
  end;

  side_angle = function(self)
    return math.pi * 2 / self.sides
  end;

  gen_triangle = function(self)
    local half_angle = self:side_angle() / 2
    local half_side_size =  math.tan(half_angle) * self.size
    return VectorList({
      Vector(0, 0),
      Vector(half_side_size, -self.size),
      Vector(-half_side_size, -self.size),
    })
  end;

  triangles = function(self)
    return range(self.sides)
    :map(function(n)
      return self:gen_triangle():rotate(self:side_angle() * n)
    end)
    :totable()
  end;

  draw = function(self, primary_color, secondary_color)
    zip(cycle({primary_color, secondary_color}), self:triangles())
    :each(function(color, triangle)
      love.graphics.setColor( color )
      love.graphics.polygon('fill', triangle:unpack())
    end)
  end;

  update = function(self, dt)
  end;
}

return Background
