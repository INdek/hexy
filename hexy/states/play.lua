-- Copyright(C) 2017, Afonso Bordado <afonsobordado@az8.co>
--
-- This program is free software; you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation; either version 2 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License along
-- with this program; if not, write to the Free Software Foundation, Inc.,
-- 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

local drawable = require "drawable"
local Vector = require "hump.vector"
local camera = require "hump.camera"
local Player = require "objects.player"
local Barrier = require "objects.barrier"
local Background = require "objects.background"
local Color = require "utils.color"
local WorldRotation = require "utils.world_rotation"
local Ui = require "hexy.ui"

local play = {}

function play:enter()
  self.ui = Ui()
  self.camera = camera.new(0, 0, 1, 0)
  self.world_rot = WorldRotation()

  self.primary_color = Color(0xFF, 0x98, 0x00, 0xFF)
  self.secondary_color = Color(0x21, 0x21, 0x21, 0xFF)

  self.primary_color:add_transform(Color.transforms.mul(self.primary_color, 0.5, 1))

  self.player = Player()
  self.background = Background(300, 6)
  self.barrier = Barrier(30, 6, 100, 400)

  self.objects = {
    self.background,
    self.player,
    self.barrier,
  }
  self.rot = 0
end


function play:draw()
  self.camera:draw(function()
    --self.world_rot:draw(function()
      each(function(obj)
        obj:draw(self.primary_color, self.secondary_color)
      end, self.objects)
    --end)
  end)

  self.ui:draw()
end

function play:update(dt)
  --self.camera:rotate(2 * dt)
  self.rot = self.rot + (1 * dt)
  self.world_rot:set_angle(math.sin(self.rot), math.cos(self.rot))

  self.primary_color:update(dt)
  self.secondary_color:update(dt)


  each(function(obj)
    obj:update(dt)
  end, self.objects)

  self.ui:update(dt)
end

function play:leave()
  self.objects = {}
end

return play
