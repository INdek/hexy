-- Copyright(C) 2017, Afonso Bordado <afonsobordado@az8.co>
--
-- This program is free software; you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation; either version 2 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License along
-- with this program; if not, write to the Free Software Foundation, Inc.,
-- 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

local Vector = require "hump.vector"
local Class = require "hump.class"
local inspect = require "inspect"
local Drawable = require "hexy.drawable.drawable"
local VectorList = require "hexy.utils.vector_list"

--     An Hexagon
--     .------.      -- TopLeft (TL) and TopRight (TR) vectors
--    /        \
--   /          \
--  .     .      .   -- Left, Center and Right vectors
--   \          /
--    \        /
--     .------.      -- BottomLeft (BL) and BottomRight (BR) vectors

local SimpleHexagon = Class {__includes = Drawable,
  init = function(self, size)
    self.size = size
  end;

  vertices = function(self)
    local half_size = self.size / 2
    local vert_dist = math.cos(math.rad(30)) * self.size
    return VectorList({
      Vector(-half_size, -vert_dist),  -- Top Left vector
      Vector(-self.size, 0), -- left vector
      Vector(-half_size, vert_dist),  -- bottom Left vector
      Vector(half_size, vert_dist),  -- bottom Right vector
      Vector(self.size, 0),  -- right vector
      Vector(half_size, -vert_dist),  -- Top Right vector
    })
  end;

  get = function(self, key)
    local vertices = self:vertices()
    local key_number = tonumber(key)
    local translation = {
      TL = 1,
      L = 2,
      BL = 3,
      BR = 4,
      R = 5,
      TR = 6,
    }

    if key_number ~= nil then
      return vertices:get(key_number)
    elseif translation[key] ~= nil then
      return vertices:get(translation[key])
    else
      error("Unkown index")
    end
  end;


  draw = function(self, position)
    assert(Vector.isvector(position))
    local vertices = self:vertices() + position
    love.graphics.polygon('fill', vertices:unpack())
  end;
}

local Hexagon = Class {__includes = Drawable,
  init = function(self, inner_size, outer_size)
    assert(inner_size < outer_size, "Inner hexagon size must be smaller than outer")
    self.inner = SimpleHexagon(inner_size)
    self.outer = SimpleHexagon(outer_size)
  end;

  edges = function(self)
    local outer = self.outer
    local inner = self.inner
    return {
      VectorList({ outer:get("TL"), inner:get("TL"), inner:get("TR"), outer:get("TR") }), -- Top Edge
      VectorList({ outer:get("L"),  inner:get("L"),  inner:get("TL"), outer:get("TL"), }), -- Top Left Edge
      VectorList({ outer:get("L"),  outer:get("BL"), inner:get("BL"), inner:get("L"),  }), -- Bottom Left Edge
      VectorList({ outer:get("BL"), outer:get("BR"), inner:get("BR"), inner:get("BL"), }), -- Bottom Edge
      VectorList({ outer:get("BR"), outer:get("R"),  inner:get("R"),  inner:get("BR"), }), -- Bottom Right Edge
      VectorList({ inner:get("R"),  outer:get("R"),  outer:get("TR"), inner:get("TR"), }), -- Top Right Edge
    }
  end;

  draw = function(self, position)
    position = position or Vector(0, 0)
    local edges = self:edges()
    each(function(vec_list)
      vec_list = vec_list + position
      love.graphics.polygon('fill', vec_list:unpack())
    end, edges)
  end;
}

return {
  Hexagon = Hexagon,
  SimpleHexagon = SimpleHexagon,
}
