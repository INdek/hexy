-- Copyright(C) 2017, Afonso Bordado <afonsobordado@az8.co>
--
-- This program is free software; you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation; either version 2 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License along
-- with this program; if not, write to the Free Software Foundation, Inc.,
-- 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

local Vector = require "hump.vector"
local Class = require "hump.class"
local inspect = require "inspect"
local Drawable = require "hexy.drawable.drawable"
local VectorList = require "hexy.utils.vector_list"

-- An Equilateral triangle
local Triangle = Class {__includes = Drawable,
  init = function(self, size)
    self.size = size
  end;

  vertices = function(self)
    local side_size = self.size
    local vertical_height = (side_size / 2) * (1 / math.cos(math.rad(30)))
    local bottom_distance = (side_size / 2) * math.tan(math.rad(30))

    return VectorList({
      Vector(0, -vertical_height),  -- Top vector
      Vector(-side_size/2, bottom_distance),  -- Bottom Left vector
      Vector(side_size/2, bottom_distance),  -- Bottom Right vector
    })
  end;

  draw = function(self, position, rotation)
    position = position or Vector(0, 0)
    rotation = rotation or 0.0
    assert(Vector.isvector(position))

    local vertices = self:vertices():rotate(rotation)
    vertices = vertices + position
    love.graphics.polygon('fill', vertices:unpack())
  end;
}

return Triangle
