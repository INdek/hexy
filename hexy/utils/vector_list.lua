-- Copyright(C) 2017, Afonso Bordado <afonsobordado@az8.co>
--
-- This program is free software; you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation; either version 2 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License along
-- with this program; if not, write to the Free Software Foundation, Inc.,
-- 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

local fun = require "fun"
local Class = require "hump.class"
local inspect = require "inspect"
local Vector = require "hump.vector"

local VectorList

local list_op_handler = function(op)
  return function(left, right)
    return VectorList(map(function(val)
      return op(val, right)
    end, left))
  end
end

VectorList = Class {
  init = function(self, initial)
    initial = initial or {}
    self:apply(initial)
  end;

  apply = function(self, table)
    enumerate(table):each(function(k, v)
      self[k] = v
    end)
    return self
  end;

  -- Unpacks this list into a list of the individual x, y coordinates
  unpack = function(self)
    return reduce(function(acc, vec)
      x, y = vec:unpack()
      table.insert(acc, x)
      table.insert(acc, y)
      return acc
    end, {}, self)
  end;

  rotate = function(self, angle)
    return self:apply(map(function(val)
      return val:rotateInplace(angle)
    end, self))
  end;

  -- unit vector is the vector of the rotation plane
  rotate_plane = function(self, unit_vector, angle)
    assert(Vector.isvector(unit_vector))
    assert(tonumber(angle) ~= nil)

    self:apply(map(function(vec)
      -- The formula used here is Rodrigues' rotation formula
      -- Vrot = V * cos(ang) + (uv * V) * sin(ang) + uv(uv . V)(1 - cos(ang))
      local s1 = vec * math.cos(angle)
      local s2 = (unit_vector:permul(vec)) * math.sin(angle)
      local s3 = unit_vector * (unit_vector * vec) * (1 - math.cos(angle))
      return s1 + s2 + s3
    end, self))

    return self
  end;

  len = function(self)
    return #self
  end;

  __tostring = function(self)
    return table.concat(map(tostring, self):totable(), ", ")
  end;

  __add = list_op_handler(fun.operator.add);
  __sub = list_op_handler(fun.operator.sub);
  __div = list_op_handler(fun.operator.div);
  __mul = list_op_handler(fun.operator.mul);
  __eq  = function(left, right)
    for _, l, r in zip(left, right) do
      if l ~= r then
        return false
      end
    end

    return true
  end;
  __len = function(self)
    return self:len()
  end;
}

return VectorList
