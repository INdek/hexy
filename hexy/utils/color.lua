-- Copyright(C) 2017, Afonso Bordado <afonsobordado@az8.co>
--
-- This program is free software; you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation; either version 2 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License along
-- with this program; if not, write to the Free Software Foundation, Inc.,
-- 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

local Class = require "hump.class"
local fun = require "fun"

local function lerp_diff(from, to, time)
  return ( to - from ) * time
end
local Color

local op_handler = function(op)
  return function(left, right)
    return Color(map(function(val)
      return op(val, right)
    end, left):totable())
  end
end


Color = Class {
  init = function(self, r, g, b, a)
    local color
    if type(r) == "table" and #r == 4 then
      color = r
    else
      color = {r, g, b, a}
    end
    self:apply(color)
    -- Transform queue
    self.queue = {}
  end;

  apply = function(self, table)
    enumerate(table):each(function(k, v)
      self[k] = v
    end)
    return self
  end;

  add_transform = function(self, transform)
    table.insert(self.queue, transform)
  end;

  remove_transform = function(self)
    table.remove(self.queue, 1)
  end;

  transform_size = function(self)
    return #self.queue
  end;

  update = function(self, dt)
    if #self.queue > 0 then
      local result = self.queue[1](self, dt)
      if result then
        self:remove_transform()
      end
    end
  end;

  approx = function(self, other, diff)
    diff = diff or 0.1
    return self[1] - other[1] < diff and
           self[2] - other[2] < diff and
           self[3] - other[3] < diff and
           self[4] - other[4] < diff
  end;

  __tostring = function(self)
    return table.concat(self, ", ")
  end;

  __add = op_handler(fun.operator.add);
  __sub = op_handler(fun.operator.sub);
  __div = op_handler(fun.operator.div);
  __mul = op_handler(fun.operator.mul);
  __eq = function(left, right)
    return left[1] == right[1] and
           left[2] == right[2] and
           left[3] == right[3] and
           left[4] == right[4]
  end;
  __len = function(self)
    return #self
  end;
}

Color.transforms = {
  mul = function(_self, amount, duration)
    local initial_color = _self:clone()
    local target_color = _self * amount
    target_color[4] = initial_color[4]

    return function(self, dt)
      self:apply(take(3, zip(initial_color, self, target_color))
        :map(function(initial, current, target)
          return current + lerp_diff(initial, target, dt / duration)
        end))

      return self:approx(target_color)
    end
  end;
}

return Color
