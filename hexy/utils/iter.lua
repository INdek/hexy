-- Copyright(C) 2017, Afonso Bordado <afonsobordado@az8.co>
--
-- This program is free software; you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation; either version 2 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License along
-- with this program; if not, write to the Free Software Foundation, Inc.,
-- 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

-- Groups the elements of the table in subtables of n size
-- skiping only by step
-- Only guaranteed to work for contiguous tables (i.e arrays)
-- TODO: We can make this better by using iterators
-- TODO: Better description
-- TODO: This isn't very pretty code
function group(t, size, step)
  assert(size >= 1, "size must not be less than 1")
  assert(step >= 1, "step must not be less than 1")
  local ret = {}

  local index = 1
  while index <= #t do
    if index+size-1 > #t then
      break
    end

    table.insert(ret, {})
    local table_key = #ret


    for size_key = 1, size do

      ret[table_key][size_key] = t[index + size_key - 1]
    end
    index = index + step
  end


  return ret
end


--for step_key = 1, iterations  do
  --ret[step_key] = {}

  --for size_key = 1, size do
  --  ret[step_key][size_key] = t[step_key + size_key - 1]
  --end
--end

return {
  group = group,
}
