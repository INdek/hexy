-- Copyright(C) 2017, Afonso Bordado <afonsobordado@az8.co>
--
-- This program is free software; you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation; either version 2 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License along
-- with this program; if not, write to the Free Software Foundation, Inc.,
-- 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


local fun = require "fun"
local Class = require "hump.class"
local inspect = require "inspect"
local Vector = require "hump.vector"

local WorldRotation = Class {
  init = function(self, initial)
    self.rot = initial or Vector(0, 0)
    assert(Vector.isvector(self.rot))
  end;

  set_angle = function(self, x, y)
    if Vector.isvector(x) then
      self.rot = x
    else
      self.rot.x = x
      self.rot.y = y
    end
  end;

  attach = function(self)
    love.graphics.push()
    love.graphics.shear(self.rot.x, self.rot.y)
  end;

  draw = function(self, fn)
    self:attach()
    fn()
    self:detach()
  end;

  detach = function(self)
    love.graphics.pop()
  end;
}

return WorldRotation
