package = "hexy"
version = "scm-1"
source = {
   url = ""
}
description = {
   summary = "SuperHexagon clone",
   detailed = [[]],
   homepage = "",
   license = "GPL-2.0"
}
dependencies = {
  'hump',
  'fun',

  -- debug deps
  'inspect >= 3.1',
  'debugger == scm-1',
}
build = {
   type = "builtin",
   modules = {
      ["hexy.draw"] = "hexy/draw.lua",
      ["hexy.drawable.drawable"] = "hexy/drawable/drawable.lua",
      ["hexy.drawable.hexagon"] = "hexy/drawable/hexagon.lua",
      ["hexy.drawable.vector_list"] = "hexy/drawable/vector_list.lua",
      ["hexy.iter"] = "hexy/iter.lua",
      ["hexy.main"] = "hexy/main.lua",
      ["tests.draw_spec"] = "tests/draw_spec.lua",
      ["tests.iter_spec"] = "tests/iter_spec.lua"
   },
   copy_directories = {
      "tests"
   }
}
