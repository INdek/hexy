SRCDIR := $(abspath hexy/)
TESTDIR := $(abspath tests/)
ROCKSFILE := hexy-scm-1.rockspec

.PHONY: run test deps coverage

run:
	love $(SRCDIR)

test:
	busted --lua=$(shell which lua) --shuffle $(TESTDIR)

coverage:
	busted --lua=$(shell which lua) --shuffle -c -Xhelper travis,env=full --verbose $(TESTDIR)

deps:
	luarocks build \
		--only-deps \
		--server=http://luarocks.org/dev \
		--local \
		$(ROCKSFILE)
