# Hexy

A super hexagon clone

You must have love2d installed
For testing the busted binary must also be available

Install dependencies with
	`make deps`

Run with
	`make`

Run tests with
	`make test`
